/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// NextcloudSpec defines the desired state of Nextcloud
type NextcloudSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Frontend defines the main frontend service
	Frontend FrontendSettings `json:"frontend,omitempty"`

	// Backend defines the main backend service
	Backend BackendSettings `json:"backend,omitempty"`

	// Cache defines the cache settings
	Cache CacheSettings `json:"cache,omitempty"`

	// DB defines the database settings
	DB DBSettings `json:"database,omitempty"`

	Resources v1.ResourceList `json:"resources,omitempty"`
	Limits    v1.ResourceList `json:"limits,omitempty"`
}

type FrontendSettings struct {
	// Image is the image to use for the frontend.
	// +kubebuilder:default="quay.io/reavessm/nextcloud-nginx:latest"
	Image string `json:"image,omitempty"`
}

type BackendSettings struct {
	// Image is the image to use for the backend.
	// +kubebuilder:default="quay.io/reavessm/nextcloud:latest"
	Image string `json:"image,omitempty"`
}

type CacheSettings struct {
	// CreateRedis determines if we should create a redis cluster.
	// +kubebuilder:default=true
	CreateCache bool `json:"create-cache,omitempty"`

	// ReadOnlyReplicas determines how many read-only replicas should be
	// initially created.  Setting this to 0 will deploy the cache in standalone
	// mode instead of boss-worker.
	// +kubebuilder:default=0
	ReadOnlyReplicas int `json:"read-only-replicas,omitempty"`

	// Port is the port used to connect to redis
	// +kubebuilder:default=5432
	Port string `json:"port"`

	// MasterImage is the container image to use for the redis master
	// +kubebuilder:default="docker.io/bitnami/valkey"
	Image string `json:"image"`

	Resources v1.ResourceList `json:"resources,omitempty"`
	Limits    v1.ResourceList `json:"limits,omitempty"`
}

type DBSettings struct {
	// +kubebuilder:default=nextcloud
	Username string `json:"username"`

	// +kubebuilder:default=nextcloud
	DBName string `json:"db-name"`

	Host string `json:"host"`

	// +kubebuilder:default=5432
	Port string `json:"port"`

	// TODO: Handle from secret
}

// NextcloudStatus defines the observed state of Nextcloud
type NextcloudStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Nextcloud is the Schema for the nextclouds API
type Nextcloud struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   NextcloudSpec   `json:"spec,omitempty"`
	Status NextcloudStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// NextcloudList contains a list of Nextcloud
type NextcloudList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Nextcloud `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Nextcloud{}, &NextcloudList{})
}

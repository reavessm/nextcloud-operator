/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/log"

	nextcloudv1alpha1 "gitlab.com/reavessm/nextcloud-operator/api/v1alpha1"
)

// NextcloudReconciler reconciles a Nextcloud object
type NextcloudReconciler struct {
	client.Client
	Recorder record.EventRecorder
	Scheme   *runtime.Scheme
}

//+kubebuilder:rbac:groups=nextcloud.reaves.dev,resources=nextclouds,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=nextcloud.reaves.dev,resources=nextclouds/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=nextcloud.reaves.dev,resources=nextclouds/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=events,verbs=create;patch
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Nextcloud object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *NextcloudReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	nextcloud := &nextcloudv1alpha1.Nextcloud{}
	err := r.Get(ctx, req.NamespacedName, nextcloud)
	if err != nil {
		if errors.IsNotFound(err) {
			// do nothing
		}
		return ctrl.Result{}, err
	}

	if nextcloud.Spec.Cache.CreateCache {
		var bossReplicaCount int32
		var workerReplicaCount int32
		bossReplicaCount = 1
		workerReplicaCount = int32(nextcloud.Spec.Cache.ReadOnlyReplicas)

		cacheBoss := appsv1.StatefulSet{
			Spec: appsv1.StatefulSetSpec{
				Replicas: &bossReplicaCount,
				Template: corev1.PodTemplateSpec{
					Spec: corev1.PodSpec{
						Containers: []corev1.Container{
							{
								Image: nextcloud.Spec.Cache.Image,
							},
						},
					},
				},
			},
		}

		writerSvc := corev1.Service{
			Spec: corev1.ServiceSpec{
				Selector: map[string]string{
					// cacheBoss.
				},
			},
		}

		cacheWorker := appsv1.StatefulSet{
			Spec: appsv1.StatefulSetSpec{
				Replicas: &workerReplicaCount,
				Template: corev1.PodTemplateSpec{
					Spec: corev1.PodSpec{
						Containers: []corev1.Container{
							{
								Image: nextcloud.Spec.Cache.Image,
								Env:   []corev1.EnvVar{
									// {
									// 	Name: ,
									// }
								},
							},
						},
					},
				},
			},
		}

		if err := r.Create(ctx, &cacheBoss); err != nil {
			return ctrl.Result{}, err
		}

		if err := r.Create(ctx, &cacheWorker); err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, err
}

// SetupWithManager sets up the controller with the Manager.
func (r *NextcloudReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&nextcloudv1alpha1.Nextcloud{}).
		Owns(&corev1.ConfigMap{}).
		Owns(&corev1.Secret{}).
		Owns(&appsv1.Deployment{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&corev1.Service{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: 2}). // ¯\_(ツ)_/¯
		Complete(r)
}
